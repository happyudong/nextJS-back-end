// import
const express = require('express');
const next = require('next');
const dev = process.env.NODE_ENV !== 'production';
const PORT = process.env.PORT || 3000;
const app = next({dir: '.', dev });
const handle = app.getRequestHandler();
const cors = require('cors');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/react_auth');
const session = require('express-session');
const jwt = require('jsonwebtoken');
const multer = require('multer');
const fs = require('fs');
// API
app.prepare().then(() => {
const server = express();
server.use(cors({origin:true}));
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(expressValidator());
let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null,__dirname + '/static/images/')
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname)
    }
});

let upload = multer({ storage: storage });

// model schema
const User = require('./static/models/user.model');

// Register & Upload Image API
server.post('/register', upload.single('displayImage'), (req, res) => {
    //console.log(req.file.originalname);
    let newUser = new User();
        newUser.email = req.body.email;
        newUser.username = req.body.username;
        newUser.password = req.body.password;
        newUser.displayImage = req.file.originalname;
        newUser.save(function (err, newUser) {
            if (err) {
              console.log(err);
              res.redirect('/')
            } else {
              res.redirect('login')
            }
        });
});

server.get('/register/:username', (req, res) => {
    const username = req.params.username;
    User.findOne({username}, (err, user)=> {
        res.send(user);
    });
});
server.get('/register', (req, res) => {
    mongoose.model('User').find().exec((err, user)=> {
        if (err) {
            res.status(404).send('404 NOT FOUND!')
        }
        else {
            res.send(user);
        }
    });
});
const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
server.use(passport.initialize());
server.use(passport.session());

passport.serializeUser(function(user, done) {
    //console.log('user', user);
    done(null, user);
});

passport.deserializeUser(function(obj, done) {
    // console.log('obj', obj);
    done(null, obj);
});
passport.use(new LocalStrategy(
    function(email, password, done) {
      User.findOne({ email: email }, function (err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        if (!user.verifyPassword(password)) { return done(null, false); }
        return done(null, user);
      });
    }
));
server.post('/login', (req, res)=> {
    if (!req.body.username || !req.body.password) {
        res.redirect('login')
    } 
    else {
        res.send(user);
    }
});
server.get('/logout', function(req, res){
    req.logout();
});
server.get('*', (req, res) => {
    return handle(req, res)
});

server.listen(PORT, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${PORT}`);
  });
});