import React from 'react';
import Link from 'next/link'
import axios from 'axios';

export default class Profile extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            users: []
        }
        this.logOut = this.logOut.bind(this);
    }
    logOut = () => {
        localStorage.removeItem('auth');
        location.href = 'login'
    }
    componentDidMount() {
        const get_Token = localStorage.getItem('auth');
        console.log(get_Token);
        axios.get(`/register/${get_Token}`).then(res => {
            const users = res.data;
            this.setState({ users });
        });
    }
    
    render() {
        let user = this.state;
        //convert object to array
        let userList = Object.values(user);
        return (
            <div>
               {
                   userList.map((user) => (
                       <ul key={user._id}>
                           <li><h2>name : {user.username}</h2></li>
                           <li><h2>mail : {user.email}</h2></li>
                           <li><h2>id : {user._id}</h2></li>
                           <img width={600} src={`static/images/${user.displayImage}`}/>
                       </ul>
                   ))
               }
                <button onClick={this.logOut} type="button">log out</button>
                <style>{`
                    li {
                        list-style:none;
                    }
                `}</style>
            </div>
        )
    }
}