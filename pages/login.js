import React from 'react';
import axios from 'axios';
import jwt from 'jsonwebtoken';
export default class PersonList extends React.Component {
  state = {
    username: [],
    password:[]
  }

  username_handle = (event) => {
    this.setState({ username: event.target.value });
  }

  password_handle = (event) => {
    this.setState({ password: event.target.value });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    axios.post(`/login`).then(res => {
      const user = {
          username: this.state.username
      }
      localStorage.setItem('auth', Object.values(user));
      if (!localStorage.getItem('auth') || localStorage.getItem('auth') === null) {
        localStorage.removeItem('auth');
        location.href = 'login'
      }
      else {
        location.href = 'profile'
      }
    })
  }
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            <input type="text" name="username" onChange={this.username_handle} />
            <br/>
            <input type="password" name="password" onChange={this.password_handle} />
          </label>
          <button type="submit">login</button>
        </form>
      </div>
    )
  }
}